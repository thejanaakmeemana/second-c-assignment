#include <stdio.h>

int main(){
  int firstNum,secondNum,temp;
  printf("enter 1st number : \n");
  scanf("%d" , &firstNum);
  printf("enter 2nd number : \n");
  scanf("%d" , &secondNum);
  printf("Before swapping 1st number is %d & 2nd number is %d \n",firstNum,secondNum);
  
  temp = firstNum;
  firstNum = secondNum;
  secondNum = temp;
  printf("After swapping 1st number is %d & 2nd number is %d \n",firstNum,secondNum);
}
